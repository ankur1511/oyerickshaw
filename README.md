Technical Assessment of OyeRickshaw

1)Assumptions i have made 
=>> Drivers latitude and longitude are available in coordinates and their coordinates keep on changing and Backend is receiving updated 
    coordinates every 5 seconds.
=>> When a customer books an e-rickshaw we keep track of it's updated coordinates to provide the location in real-time to the customer.
=>> When user try to book an ride we search all the e-rickshaw available within a circle of certain radius.

2)Approach
=>> We know the coordinates of the customer when they login to the App and when user hit the book ride button we try to 
    search all the e-rickshaw available within a certain radius along with their availability and show it to the customer on UI. 
    (for this we need to expose an API to GET all the drivers available)
=>> Now when customer booked the ride user needs to know the current location of the ride. (This need to expose an API to know
    the updated location of ride in real-time)
=>> As we recieve the updated coordinates of drivers every 5 seconds we need to expose an API that updates the drivers coordinates.

3)Steps to run this Application
=>> This is a Spring boot project which is standalone and having embedded tomcat server in it, and maven has been used as a build management
    tool, so you need to install maven on your machine and should have  JDK 8 installed to build this project.
a) Clone this reporsitory and go to the directory of your project from terminal.
b) build the project using command mvn clean install
c) after building the project you will have a oyerickshaw-0.0.1-SNAPSHOT.jar file created at path /oyerickshaw/target.
d) run java -jar oyerickshaw-0.0.1-SNAPSHOT.jar on terminal, and it will RUN your project.

And if you have Intellij Idea IDE then just import this project and Run it.

