package com.ankur.oyerickshaw.Utils;

import com.ankur.oyerickshaw.Beans.Coordinates;
import java.util.HashMap;
import java.util.Map;

public class RickshawUtils {

    /**
     * This method is used to populate initial coordinates of drivers
     * @return a map of driver Id and their corresponding coordinates
     */
    public static Map<Integer, Coordinates> populateDriverLocationCoordinates(){

        Map<Integer, Coordinates> map = new HashMap<>();

        map.put(1, new Coordinates(17.1f, 20.8f));
        map.put(2, new Coordinates(11.5f, 16.9f));
        map.put(3, new Coordinates(7.1f, 12.9f));
        map.put(4, new Coordinates(15.3f, 22.6f));
        map.put(5, new Coordinates(3.7f, 13.4f));
        map.put(6, new Coordinates(70.3f, 45.8f));

        return map;
    }

    /**
     * This method is used to determine if a driver lies under a circle of given radius
     * @param radius
     * @param latitude
     * @param longitude
     * @param coordinates
     * @return true or false
     */
    public static boolean checkDriverUnderRadius(Integer radius, Float latitude, Float longitude, Coordinates coordinates){

        float x = Math.abs(latitude-coordinates.getLatitude()) * Math.abs(latitude-coordinates.getLatitude());
        float y = Math.abs(longitude-coordinates.getLongitude()) * Math.abs(longitude-coordinates.getLongitude());

        return (int)Math.sqrt(x + y) < radius;
    }

}
