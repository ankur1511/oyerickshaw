package com.ankur.oyerickshaw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OyerickshawApplication {

	public static void main(String[] args) {
		SpringApplication.run(OyerickshawApplication.class, args);
	}

}

