package com.ankur.oyerickshaw.service;

import com.ankur.oyerickshaw.Beans.Coordinates;
import com.ankur.oyerickshaw.Beans.DriverInfo;
import com.ankur.oyerickshaw.Beans.Response;
import com.ankur.oyerickshaw.Utils.RickshawUtils;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class TrackingServiceImpl implements TrackingService {

    private Map<Integer, Coordinates> coordinatesMap;

    @PostConstruct
    void populateLocations(){
        coordinatesMap = RickshawUtils.populateDriverLocationCoordinates();
    }

    @Override
    public Response getAllDriversLocation() {

        Response response = new Response();
        List<DriverInfo> driverInfoList = new ArrayList<>();

        coordinatesMap.keySet().forEach( driverId -> {
             driverInfoList.add(new DriverInfo(driverId, coordinatesMap.get(driverId)));
        });

        response.setDriverInfoList(driverInfoList);
        return response;
    }

    @Override
    public Response getDriverLocationById(Integer driverId) {

        Response response = new Response();

        if(coordinatesMap.get(driverId) != null){
            List<DriverInfo> driverInfo = new ArrayList<>();
            driverInfo.add(new DriverInfo(driverId, coordinatesMap.get(driverId)));
            response.setDriverInfoList(driverInfo);
        }else {
            response.setError("No Driver Found with given driver Id");
        }
        return response;
    }

    @Override
    public Response getAllDriverInRadius(Integer radius, Float latitude, Float longitude) {

        Response response = new Response();
        List<DriverInfo> driverInfoList;
        driverInfoList = findDriversInRadius(radius, latitude, longitude);

        if(!driverInfoList.isEmpty()){
            response.setDriverInfoList(driverInfoList);
        }else {
            response.setError("Oop! No Driver Found");
        }
        return response;
    }

    @Override
    public void updateDriverCoordinate(DriverInfo driverInfo) {
        coordinatesMap.put(driverInfo.getUserId(), driverInfo.getDriverLocation());
    }

    private List<DriverInfo> findDriversInRadius(Integer radius, Float latitude, Float longitude){

        List<DriverInfo> driverInfoList = new ArrayList<>();
        coordinatesMap.keySet().forEach( driverId -> {
            if(RickshawUtils.checkDriverUnderRadius(radius, latitude, longitude, coordinatesMap.get(driverId))){
                driverInfoList.add(new DriverInfo(driverId, coordinatesMap.get(driverId)));
            }
        });

        return driverInfoList;
    }
}
