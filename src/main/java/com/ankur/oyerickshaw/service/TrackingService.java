package com.ankur.oyerickshaw.service;

import com.ankur.oyerickshaw.Beans.DriverInfo;
import com.ankur.oyerickshaw.Beans.Response;

public interface TrackingService {

    Response getAllDriversLocation();

    Response getDriverLocationById(Integer userId);

    Response getAllDriverInRadius(Integer radius, Float latitude, Float longitude);

    void updateDriverCoordinate(DriverInfo driverInfo);

}
