package com.ankur.oyerickshaw.Beans;

public class DriverInfo {

    Integer userId;
    Coordinates driverLocation;

    public DriverInfo(){
        //do nothing
    }

    public DriverInfo(Integer userId, Coordinates driverLocation) {
        this.userId = userId;
        this.driverLocation = driverLocation;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Coordinates getDriverLocation() {
        return driverLocation;
    }

    public void setDriverLocation(Coordinates driverLocation) {
        this.driverLocation = driverLocation;
    }
}
