package com.ankur.oyerickshaw.Beans;

import java.util.List;

public class Response {

    List<DriverInfo> driverInfoList;
    String error;

    public List<DriverInfo> getDriverInfoList() {
        return driverInfoList;
    }

    public void setDriverInfoList(List<DriverInfo> driverInfoList) {
        this.driverInfoList = driverInfoList;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
