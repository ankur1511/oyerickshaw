package com.ankur.oyerickshaw.controllers;

import com.ankur.oyerickshaw.Beans.DriverInfo;
import com.ankur.oyerickshaw.Beans.Response;
import com.ankur.oyerickshaw.service.TrackingService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "/api/oyerickshaw/v1", produces = {APPLICATION_JSON_VALUE})
public class RickshawController {

    @Resource
    TrackingService trackingService;

    @RequestMapping(value = "/drivers", method = RequestMethod.GET)
    public ResponseEntity<Response> getAllDriversLocation(){

        Response res = trackingService.getAllDriversLocation();
        return new ResponseEntity<>(res, HttpStatus.OK);
    }

    @RequestMapping(value = "/{driverId}", method = RequestMethod.GET)
    public ResponseEntity<Response> getDriverLocationById(@PathVariable(value = "driverId") Integer driverId){

        Response response = trackingService.getDriverLocationById(driverId);
        if(!response.getError().isEmpty()){
            return new ResponseEntity<>(response, HttpStatus.OK);
        }else {
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @RequestMapping(value = "/radius", method = RequestMethod.GET)
    public ResponseEntity<Response> getDriversWithinRadius(@RequestParam(value = "radius") Integer radius,
                                                           @RequestParam("latitude") Float latitude,
                                                           @RequestParam("longitude") Float longitude){

        Response response = trackingService.getAllDriverInRadius(radius, latitude, longitude);
        if(!response.getError().isEmpty()){
            return new ResponseEntity<>(response, HttpStatus.OK);
        }else {
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity updateDriverCoordinates(@RequestBody DriverInfo driverInfo){
        trackingService.updateDriverCoordinate(driverInfo);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
